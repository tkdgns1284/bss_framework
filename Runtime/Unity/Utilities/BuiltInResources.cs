﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public enum BuiltInSpriteType {
        Transparent = 0,
        Box = 1,
        Circle = 2,
    }

    public static class BuiltInResources {
        public static Sprite GetSprite(BuiltInSpriteType sprType) {
            string path = "";
            switch (sprType) {
                case BuiltInSpriteType.Transparent:
                    path= "BSS_Transparent";
                    break;
                case BuiltInSpriteType.Box:
                    path = "BSS_Box";
                    break;
                case BuiltInSpriteType.Circle:
                    path = "BSS_Circle";
                    break;
                default:
                    break;
            }
            return Resources.Load<Sprite>(path);
        }
    }
}
