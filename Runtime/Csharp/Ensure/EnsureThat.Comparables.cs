﻿using System;

namespace BSS
{
	public partial class EnsureThat
	{
		public void Is<T>(T param, T expected) where T : struct, IComparable<T>
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (!param.IsEqual(expected))
			{
				throw new ArgumentException(ExceptionMessages.Comp_Is_Failed.Inject(param, expected), paramName);
			}
		}

		public void IsNot<T>(T param, T expected) where T : struct, IComparable<T>
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (param.IsEqual(expected))
			{
				throw new ArgumentException(ExceptionMessages.Comp_IsNot_Failed.Inject(param, expected), paramName);
			}
		}

		public void IsLess<T>(T param, T limit) where T : struct, IComparable<T>
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (!param.IsLessThan(limit))
			{
				throw new ArgumentException(ExceptionMessages.Comp_IsNotLt.Inject(param, limit), paramName);
			}
		}

		public void IsLessOrEqual<T>(T param, T limit) where T : struct, IComparable<T>
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (param.IsGreaterThan(limit))
			{
				throw new ArgumentException(ExceptionMessages.Comp_IsNotLte.Inject(param, limit), paramName);
			}
		}

		public void IsGreater<T>(T param, T limit) where T : struct, IComparable<T>
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (!param.IsGreaterThan(limit))
			{
				throw new ArgumentException(ExceptionMessages.Comp_IsNotGt.Inject(param, limit), paramName);
			}
		}

		public void IsGreaterOrEqual<T>(T param, T limit) where T : struct, IComparable<T>
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (param.IsLessThan(limit))
			{
				throw new ArgumentException(ExceptionMessages.Comp_IsNotGte.Inject(param, limit), paramName);
			}
		}

		public void IsInRange<T>(T param, T min, T exclusiveMax) where T : struct, IComparable<T>
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (param.IsLessThan(min))
			{
				throw new ArgumentException(ExceptionMessages.Comp_IsNotInRange_ToLow.Inject(param, min), paramName);
			}

			if (param.IsEqual(exclusiveMax) || param.IsGreaterThan(exclusiveMax))
			{
				throw new ArgumentException(ExceptionMessages.Comp_IsNotInRange_ToHigh.Inject(param, exclusiveMax), paramName);
			}
		}

        public void IsInRangeInclusive<T>(T param, T min, T inclusiveMax) where T : struct, IComparable<T> {
            if(!Ensure.IsActive) {
                return;
            }

            if(param.IsLessThan(min)) {
                throw new ArgumentException(ExceptionMessages.Comp_IsNotInRange_ToLow.Inject(param, min), paramName);
            }

            if(param.IsGreaterThan(inclusiveMax)) {
                throw new ArgumentException(ExceptionMessages.Comp_IsNotInRange_ToHigh.Inject(param, inclusiveMax), paramName);
            }
        }
    }
}