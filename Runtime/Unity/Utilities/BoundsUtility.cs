﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class BoundsUtility {
        public static Rect ToRect(this Bounds bounds) {
            return new Rect(bounds.min, bounds.size);
        }

    }

}