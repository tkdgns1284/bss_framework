﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;
using System.Linq;

namespace BSS.View {
	[RequireComponent (typeof(CanvasGroup))]
	public class BaseView : MonoBehaviour
	{
		[SerializeField]
		private string id;

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string ID {
			get{ return id; }
			set{ id = value; }
		}

        [FoldoutGroup("Element")]
        [SerializeField]
        public Button closeButton;

        [FoldoutGroup("Show Event")]
        public AudioClip showSound;
        [FoldoutGroup("Show Event")]
        public TweenElement showTween;

        [FoldoutGroup("Close Event")]
        public AudioClip closeSound;
        [FoldoutGroup("Close Event")]
        public TweenElement closeTween;

        public event Action OnShowAction;
        public event Action OnCloseAction;

        public bool IsVisible { 
			get { 
				return canvasGroup.alpha == 1f; 
			} 
		}
        public int sortingOrder => parentCanvas.sortingOrder;
        public string sortingLayerName => parentCanvas.sortingLayerName;
        public int sortingLayerID => parentCanvas.sortingLayerID;

        protected Canvas parentCanvas;  
		protected RectTransform rectTransform;
		protected CanvasGroup canvasGroup {
            get { return GetComponent<CanvasGroup>(); }
        }

        private void Awake ()
		{
            parentCanvas = GetComponentInParent<Canvas>();
            rectTransform = GetComponent<RectTransform> ();
            if (closeButton != null)  closeButton.Listen(Close);
            OnAwake();
		}
        private void OnEnable() {
            ViewUtility.baseViewAll.Add(this);
        }
        private void OnDisable() {
            ViewUtility.baseViewAll.Remove(this);
        }

        protected virtual void OnAwake ()
		{
		}

		private void Start ()
		{
			OnStart ();
		}

		protected virtual void OnStart ()
		{
		}

		public virtual void Show ()
		{
            SoundSystem.PlayOnce (showSound, 1.0f);
			canvasGroup.interactable = true;
			canvasGroup.blocksRaycasts = true;
            if (showTween == null) {
                canvasGroup.alpha = 1f;
            } else {
                if (!showTween.isAlpha) {
                    canvasGroup.alpha = 1f;
                } 
                TweenUtility.SetTween(gameObject, showTween,1, () => {
                    if (canvasGroup != null) {
                        canvasGroup.alpha = 1f;
                    }
                });
            }

            if (OnShowAction != null) {
                OnShowAction.Invoke ();
			}
		}
        [ButtonGroup("ShowClose")]
        [Button(Name = "Show")]
        public void ShowByForce() {
            canvasGroup.alpha = 1f;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }

		public virtual void Close ()
		{
            SoundSystem.PlayOnce (closeSound, 1.0f);
			canvasGroup.interactable = false;
			canvasGroup.blocksRaycasts = false;
            if (closeTween == null) {
                canvasGroup.alpha = 0f;
            } else {
                if (!closeTween.isAlpha) {
                    canvasGroup.alpha = 0f;
                }
                TweenUtility.SetTween(gameObject, closeTween,1, () => {
                    if (canvasGroup != null) {
                        canvasGroup.alpha = 0f;
                    }
                });
            }

            if (OnCloseAction != null) {
                OnCloseAction.Invoke ();
			}
		}
        [ButtonGroup("ShowClose")]
        [Button(Name ="Close")]
        public void CloseByForce() {
            canvasGroup.alpha = 0f;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        public virtual void Toggle ()
		{
			if (!IsVisible) {
				Show ();
			} else {
				Close ();
			}
		}

        public T NextView<T>(bool selfClose=false) where T : BaseView {
            var view = ViewUtility.Find<T>();
            view.Show();
            if (selfClose) {
                Close();
            }
            return view;
        }

		public virtual void Focus ()
		{
			rectTransform.SetAsLastSibling ();
		}
        
    }
}