﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace BSS {
    [System.Serializable]
    public class WorldTextInfo {
        [Header("Canvas")]
        public Vector2 canvasScale = Vector2.one * 0.01f;
        public Vector2 canvasSize = new Vector2(200f, 200f);
        public Vector2 relativePosition;
        public string sortingLayerName = "Default";
        public int sortingOrder;

        [Header("Font")]
        public Font font;
        public int fontSize = 10;
        public Color fontColor = Color.white;
        public TextAnchor alignment = TextAnchor.MiddleCenter;
    }
    /// <summary>
    /// GameObject에 Text를 표시 할 수 있게 해주는 클래스
    /// Regist(시작) -> SetContent -> UnRegist(종료)
    /// </summary>
    public class WorldTextDrawer : Singleton<WorldTextDrawer> {
        private Dictionary<GameObject, Canvas> canvasDics = new Dictionary<GameObject, Canvas>();

        public static bool IsRegist(GameObject obj) {
            return instance.canvasDics.ContainsKey(obj);
        }
        public static Canvas GetCanvas(GameObject obj) {
            return instance.canvasDics[obj];
        }
        public static Text GetText(GameObject obj) {
            return instance.canvasDics[obj].transform.GetChild(0).GetComponent<Text>();
        }
        public static void SetContent(GameObject obj,string content) {
            GetText(obj).text = content;
        }

        /// <summary>
        /// 게임오브젝트에 자식으로 Canvas와 Text를 생성하고 등록합니다.
        /// </summary>
        public static void Regist(GameObject obj,WorldTextInfo info) {
            if (instance.canvasDics.ContainsKey(obj)) {
                UnRegist(obj);
            }
            
            var canvas= Go.New("Canvas").SetParent(obj, info.relativePosition).Attach<Canvas>();
            canvas.transform.localScale = info.canvasScale;
            canvas.GetComponent<RectTransform>().SetSize(info.canvasSize);
            canvas.worldCamera = Camera.main;
            canvas.sortingLayerName = info.sortingLayerName;
            canvas.sortingOrder = info.sortingOrder;
            var text = Go.New("Text").SetParent(canvas.transform).Attach<Text>();
            text.font = info.font;
            text.fontSize = info.fontSize;
            text.color = info.fontColor;
            text.alignment = info.alignment;
            text.GetComponent<RectTransform>().SetFullSize();
            text.raycastTarget = false;
            instance.canvasDics[obj] = canvas;
        }

        /// <summary>
        /// 게임오브젝트의 Canvas와 Text를 제거하고 해제합니다. 
        /// </summary>
        public static void UnRegist(GameObject obj) {
            if (!instance.canvasDics.ContainsKey(obj)) return;
            if (instance.canvasDics[obj] != null) {
                Destroy(instance.canvasDics[obj].gameObject);
            }
            instance.canvasDics.Remove(obj);
        }
    }
}
