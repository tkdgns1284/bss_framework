﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class CopyUtility 
    {
        public static SpriteRenderer DeepCopy(this SpriteRenderer sprRender, SpriteRenderer other) {
            sprRender.sprite = other.sprite;
            sprRender.color = other.color;
            sprRender.flipX = other.flipX;
            sprRender.flipY = other.flipY;
            sprRender.material = other.material;
            sprRender.drawMode = other.drawMode;
            sprRender.sortingLayerID = other.sortingLayerID;
            sprRender.sortingOrder = other.sortingOrder;
            sprRender.maskInteraction = other.maskInteraction;
            sprRender.spriteSortPoint = other.spriteSortPoint;
            return sprRender;
        }
        public static Transform DeepCopyTr(this Transform tr, Transform other) {
            tr.position = other.position;
            tr.rotation = other.rotation;
            tr.localScale = other.localScale;
            return tr;
        }
        public static RectTransform DeepCopyRectTr(this RectTransform rectTr,RectTransform other) {
            rectTr.anchorMin = other.anchorMin;
            rectTr.anchorMax = other.anchorMax;
            rectTr.anchoredPosition = other.anchoredPosition;
            rectTr.sizeDelta = other.sizeDelta;
            return rectTr;
        }
        public static RectTransform DeepCopyRectTrWithCanvas(this RectTransform rectTr, RectTransform other) {
            var otherCanvas = other.GetComponentInParent<Canvas>();
            rectTr.SetParent(otherCanvas.transform, false);
            rectTr.anchorMin = other.anchorMin;
            rectTr.anchorMax = other.anchorMax;
            rectTr.anchoredPosition = other.anchoredPosition;
            rectTr.sizeDelta = other.sizeDelta;
            return rectTr;
        }
    }

}