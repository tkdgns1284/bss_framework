BSS 프레임워크
------------------
BSS는 유니티 전용 프레임워크이며 2D 게임을 기반으로 제작되었습니다.

# Specification
- Unity 2019 이상
- Script .Net 4.x 이상

# Features
- UI System (UGUI-based, Show/Close, Custom Editor)
- 2D Animation System (Custom Editor,Preview,ScriptableObject)
- Tween System (Graph Curve,ScriptableObject)
- Sound System (Simple & Easy, Android native support)
- Move System (Target Follow,Move to goal)
- Productive Additional Component (Ex: Joystick,GameObject Factory,WolrdText ...)
- Useful Unity Extension Method 
- ETC

# Dependencies (Built-in)
- Odin Inspector
- Async Coroutine 

------------------
## Create By
- __Lee Sang Hun__
- __Email__: tkdgns1284@gmail.com
- __Gitlab__: https://gitlab.com/tkdgns1284