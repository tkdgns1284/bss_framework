﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace BSS {
    /// <summary>
    /// Scene 전환시 제거되는 싱글톤 (없을경우 생성하지 않음)
    /// </summary>
    public class PassiveSingleton<T> : MonoBehaviour where T : MonoBehaviour {
        private static T _instance;
        public static T instance {
            get {
                if (_instance == null) {
                    var insArray= FindObjectsOfType<T>(); 
                    if (insArray.Length == 0) throw new Exception($"[{typeof(T).Name}] is not exist.");
                    if (insArray.Length > 1) throw new Exception($"[{typeof(T).Name}] is not single.");
                    _instance = insArray[0];
                }
                return _instance;
            }
        }
    }
}
