﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class RectUtility 
    {
        /// <summary>
        /// Rect를 특정 사이즈로 균등하게 자른 후 해당 위치의 Rect를 가져옵니다.
        /// 시작 위치(0,0)는 왼쪽 아래 기준
        /// </summary>
        public static Rect Split(this Rect rect, int splitX, int splitY, int x, int y) {
            Ensure.That(nameof(rect.size.x)).IsGreater(rect.size.x, 0);
            Ensure.That(nameof(rect.size.y)).IsGreater(rect.size.y, 0);
            Ensure.That(nameof(splitX)).IsGreater(splitX, 0);
            Ensure.That(nameof(splitY)).IsGreater(splitY, 0);
            Ensure.That(nameof(x)).IsInRange(x, 0, splitX);
            Ensure.That(nameof(y)).IsInRange(y, 0, splitY);
            Vector2 rectSize = VectorUtility.InverseScale(rect.size, new Vector2(splitX, splitY));
            Vector2 firstPos = (Vector2)rect.min + Vector2.Scale(rectSize, new Vector2(x,y));
            return new Rect(firstPos, rectSize);
        }
    }
}
