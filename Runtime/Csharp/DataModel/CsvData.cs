﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System;

namespace BSS {
    /// <summary>
    /// CSV파일의 정보를 가지고 있는 Class (첫번째 행은 반드시 Key 값)
    /// </summary>
    [System.Serializable]
    public class CsvData {
        static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
        static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";

        public List<string> Keys;
        public List<Dictionary<string, string>> Maps = new List<Dictionary<string, string>>();
        public Dictionary<string, List<string>> Values = new Dictionary<string, List<string>>();


        public CsvData(string csvFormat) {
            string[] lines = Regex.Split(csvFormat, LINE_SPLIT_RE);

            Keys = Regex.Split(lines[0], SPLIT_RE).ToList();
            foreach (var key in Keys) {
                Values[key] = new List<string>();
            }

            for (var i = 1; i < lines.Length; i++) {
                if (string.IsNullOrEmpty(lines[i])) continue;
                var dic = new Dictionary<string, string>();
                string[] values = Regex.Split(lines[i], SPLIT_RE);
                for (int j = 0; j < values.Length; j++) {
                    string key = Keys[j];
                    string value = values[j];
                    dic[key] = value;
                    Values[key].Add(value);
                }
                Maps.Add(dic);
            }
        }

        public Dictionary<string, string> GetRowData(string key,Func<string,bool> predicate) {
            int index=Values[key].FindIndex(x => predicate(x));
            if (index == -1) return null;
            return Maps[index];
        }
        public List<Dictionary<string, string>> GetRowDataAll(string key, Func<string, bool> predicate) {
            var keyValues=Values[key].FindAll(x => predicate(x));
            return Maps.FindAll(x => keyValues.Exists(xx => xx == x[key]));
        }
    }
}
