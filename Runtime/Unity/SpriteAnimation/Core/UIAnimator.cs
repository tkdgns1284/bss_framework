﻿using UnityEngine;
using UnityEngine.UI;

namespace BSS {
    /// <summary>
    /// Animator for Image from the Unity UI system.
    /// </summary>
    [AddComponentMenu("Spritedow/UI Animator")]
    [RequireComponent(typeof(Image))]
    [DisallowMultipleComponent]
    public class UIAnimator : BaseAnimator
    {
        private Vector2 initSize;
        public Image image {
            get { return GetComponent<Image>(); }
        }
    

        protected override void Awake()
        {
            initSize = image.rectTransform.sizeDelta;
            base.Awake();
        }

        /// <summary>
        /// Changes the sprite to the given sprite.
        /// </summary>
        protected override void ChangeFrame(Sprite frame)
        {
            image.sprite = frame;
        }

    }
}