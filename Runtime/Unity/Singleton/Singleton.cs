﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BSS {
    /// <summary>
    /// Scene 전환시 제거되는 싱글톤 (없을경우 생성)
    /// </summary>
    public class Singleton<T> : MonoBehaviour where T: MonoBehaviour {
        private static T _instance;
        public static T instance {
            get {
                if (_instance == null) {
                    var insArray = FindObjectsOfType<T>();
                    
                    if (insArray.Length > 1) throw new Exception($"[{typeof(T).Name}] is not single.");
                    if (insArray.Length == 0) {
                        var obj = new GameObject(typeof(T).Name);
                        _instance = obj.AddComponent<T>();
                    } else if (insArray.Length == 1) {
                        _instance = insArray[0];
                    }
                }
                return _instance;
            }
        }
    }

    
}
