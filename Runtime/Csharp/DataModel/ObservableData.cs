﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace BSS {
    public class ObservableData<T> {
        private T _data;
        public T data {
            get => _data;
            set {
                Change(value);
            }
        }
        public event Action<T> OnChanged;
        public void Change(T setData) {
            if(setData == null && _data == null)
                return;
            if(setData != null && setData.Equals(_data))
                return;
            _data = setData;
            OnChanged?.Invoke(_data);
        }
    }
}
