﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BSS;
using System.Linq;

namespace BSS {
    [System.Serializable]
    public class LineInfo {
        public float size = 1f;
        public Color color = Color.white;
        public Material material;
        public string sortingLayerName = "Default";
        public int sortingOrder;
    }

    public class LineDrawer : PassiveSingleton<LineDrawer> {
        public int poolCount = 5;
        public LineInfo defaultLineInfo = new LineInfo();

        private List<LineRenderer> lineRenderers = new List<LineRenderer>();


        private void Awake() {
            for (int i = 0; i < poolCount; i++) {
                CreateLineRender();
            }
        }
        public static int Draw(params Vector3[] positions) {
            if (positions.Length < 2) throw new System.Exception("유효범위 초과");
            var index = instance.lineRenderers.FindIndex(x => !x.gameObject.activeSelf);
            if (index == -1) {
                index = instance.CreateLineRender();
            }
            var render = instance.lineRenderers[index];
            instance.ApplyLineInfo(render, instance.defaultLineInfo,instance.defaultLineInfo.color);

            render.gameObject.SetActive(true);
            render.positionCount = positions.Length;
            render.SetPositions(positions);
            return index;
        }

        public static int Draw(List<Vector3> positions) {
            return Draw(positions.ToArray());
        }

        public static int DrawColor(Color setColor,params Vector3[] positions) {
            if (positions.Length < 2) throw new System.Exception("유효범위 초과");
            var index = instance.lineRenderers.FindIndex(x => !x.gameObject.activeSelf);
            if (index == -1) {
                index = instance.CreateLineRender();
            }
            var render = instance.lineRenderers[index];
            instance.ApplyLineInfo(render, instance.defaultLineInfo, setColor);

            render.gameObject.SetActive(true);
            render.positionCount = positions.Length;
            render.SetPositions(positions);
            return index;
        }
        public static int DrawColor(Color setColor, List<Vector3> positions) {
            return DrawColor(setColor,positions.ToArray());
        }

        public static void Erase(int index) {
            var render = instance.lineRenderers[index];
            render.gameObject.SetActive(false);
        }
        public static void Clear() {
            for (int i = 0; i < instance.lineRenderers.Count; i++) {
                instance.lineRenderers[i].gameObject.SetActive(false);
            }
        }

        private void ApplyLineInfo(LineRenderer render,LineInfo lineInfo,Color setColor) {
            render.startColor = setColor;
            render.endColor = setColor;
            render.startWidth = lineInfo.size;
            render.endWidth = lineInfo.size;
            render.material = lineInfo.material;
            render.sortingLayerName = lineInfo.sortingLayerName;
            render.sortingOrder = lineInfo.sortingOrder;
        }

        private int CreateLineRender() {
            var render = Go.New("LineRender" + lineRenderers.Count.ToString()).SetParent(transform).Attach<LineRenderer>();
            render.useWorldSpace = true;
            lineRenderers.Add(render);
            render.gameObject.SetActive(false);
            return lineRenderers.Count - 1;
        }
    }
}
