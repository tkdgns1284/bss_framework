﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace BSS.View {
    /// <summary>
    /// Tab버튼으로 페이지를 바꾸는 컴포넌트
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BaseView))]
    public class TabBar : MonoBehaviour {
        [System.Serializable]
        public class TabInfo {
            public Button button;
            public GameObject activeObj;
        }
        public int selectIndex {
            set {
                if (value < 0) return;
                if (value >= tabInfos.Count) return;
                _selectIndex = value;
                ApplyActive();
            }
            get => _selectIndex;
        }
        [SerializeField]
        private int _selectIndex;
        public List<TabInfo> tabInfos = new List<TabInfo>();

        public Action<Button> OnTabButtonPressed;

        private void Start() {
            for (int i = 0; i < tabInfos.Count; i++) {
                int index = i;
                tabInfos[i].button.Listen(() => {
                    var button = tabInfos[index].button;
                    selectIndex = tabInfos.FindIndex(x => x.button == button);
                    OnTabButtonPressed?.Invoke(button);
                });
            }
            if (tabInfos.Count > 0) {
                ApplyActive();
            }
        }

        private void ApplyActive() {
            var tabInfo = tabInfos[selectIndex];
            foreach (var it in tabInfos) {
                it.activeObj.SetActive(tabInfo == it);
            }
        }
    }
}
