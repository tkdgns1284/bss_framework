﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace BSS.View {
    /// <summary>
    /// Up,Down버튼으로 페이지를 바꾸는 컴포넌트
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BaseView))]
    public class PageBar : MonoBehaviour {
        public int selectIndex {
            set {
                if (selectIndex == value) return;
                if (value < 0) return;
                if (value >= pageActiveObjs.Count) return;
                _selectIndex = value;
                ApplyActive();
            }
            get => _selectIndex;
        }
        public bool isShowInit = true;
        [SerializeField]
        private int _selectIndex;
        public Button downButton;
        public Button upButton;
        public List<GameObject> pageActiveObjs = new List<GameObject>();

        public Action<Button> OnDownButtonPressed;
        public Action<Button> OnUpButtonPressed;

        private void Start() {
            downButton.Listen(() => {
                selectIndex -= 1;
                OnDownButtonPressed?.Invoke(downButton);
            });
            upButton.Listen(() => {
                selectIndex += 1;
                OnUpButtonPressed?.Invoke(upButton);
            });
            GetComponent<BaseView>().OnShowAction+=() => {
                selectIndex = 0;
            };
            if (pageActiveObjs.Count > 0) {
                ApplyActive();
            }
        }

        private void ApplyActive() {
            for (int i=0;i< pageActiveObjs.Count;i++) {
                pageActiveObjs[i].SetActive(i == selectIndex);
            }
        }
    }
}
